/// <reference lib="dom"/>
// deno-lint-ignore-file no-unused-vars

let roomName: string
let roomInput: HTMLInputElement

globalThis.onload = () => {
    roomInput = document.getElementById("room")! as HTMLInputElement
    roomName = generateRoomName(8)
    roomInput.placeholder += roomName
}

function generateRoomName(length: number) {
    let result = ""
    const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    let counter = 0
    while (counter < length) {
      result += characters.charAt(Math.floor(Math.random() * characters.length))
      counter += 1
    }
    return result
}

function joinRoom() {
    window.location.href = "/room/" + encodeURIComponent(roomInput.value == "" ? roomName : roomInput.value)
}