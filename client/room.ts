// deno-lint-ignore-file no-unused-vars no-explicit-any
const webSocket = new WebSocket("/wss")
const roomName = window.location.pathname.split("/").pop()
const username = getUsername()

const video = <HTMLVideoElement>document.getElementById("player")!
const urlInput = <HTMLInputElement>document.getElementById("url")!
const waitingPopup = document.getElementById("waiting-popup")!
const errorPopup = document.getElementById("error-popup")!
const autoplayPopup = document.getElementById("autoplay-popup")!
const unameInp = <HTMLInputElement>document.getElementById("usernameInp")!

let roomInfo: RoomInfo
let myId: number

let remotePlayCommand = false
let popupShown = false

let lastTimeUpdate = Date.now()

interface RoomInfo {
    users: User[]
    video: string | undefined
    progress: number
    playing: boolean
    playingSince: number
    key: string | undefined
    waiting: boolean
}

interface User {
    name: string
    uuid: string
}

globalThis.onload = () => {
    unameInp.textContent = username
    unameInp.onkeydown = (e) => {
		if (e.key === "Enter") e.preventDefault()
	}
    unameInp.oninput = _e => {
        webSocket.send(createPacket("SetUsername", unameInp.textContent))
    }

    video.onplaying = (_event) => {
        if (remotePlayCommand) {
            remotePlayCommand = false
            return
        }
        roomInfo.playing = true
        roomInfo.playingSince = Date.now()
        webSocket.send(createPacket("PlayState", true))
    }
    video.onpause = (_event) => {
        if (remotePlayCommand) {
            remotePlayCommand = false
            return
        }
        roomInfo.playing = false
        roomInfo.playingSince = Date.now()
        webSocket.send(createPacket("PlayState", false))
    }
    video.onseeking = (_event) => {
        if (remotePlayCommand) {
            remotePlayCommand = false
            return
        }
        const t = Math.round(video.currentTime * 1000)
        webSocket.send(createPacket("Seek", t))
        roomInfo.progress = t
    }
    video.onwaiting = (_event) => {
        if (roomInfo.playing) {
            roomInfo.playing = false
            roomInfo.playingSince = Date.now()
            webSocket.send(createPacket("PlayState", false))
        }
    }
    video.ontimeupdate = (_event) => {
        if (myId == null) return
        const now = Date.now()
        if (now - lastTimeUpdate > 200) {
            lastTimeUpdate = now
            const progressElem = document.getElementById(myId + "-progress")!
            progressElem.innerText = formatTime(video.currentTime)
            webSocket.send(createPacket("TimeUpdate", video.currentTime))
        }
    }
    urlInput.addEventListener("keypress", (e) => {
        if (e.key === "Enter") {
            submitVideo()
        }
    })
    try {
        if (navigator.getAutoplayPolicy(video) != "allowed") {
            autoplayPopupShow()
            popupShown = true
        }
    } catch {
        // getAutoplayPolicy is still unsupported on all major browsers except for Firefox.
        autoplayPopupShow()
        popupShown = true
    }
}

webSocket.onopen = _event => webSocket.send(createPacket("JoinRoom", { "room": roomName, "username": username }))

webSocket.onerror = _event => console.error("WebSocket error!")
webSocket.onmessage = event => {
    const json = JSON.parse(event.data)
    console.log("received packet:", json)
    switch (json.type) {
        case "RoomInfo": handleRoomInfo(json.data); break
        case "UserJoin": handleUserJoin(json.data); break
        case "UserLeave": handleUserLeave(json.data); break
        case "SetUsername": handleSetUsername(json.data); break
        case "SetVideo": handleSetVideo(json.data); break
        case "UserProgress": handleUserProgress(json.data); break
        case "Wait": handleWait(); break
        case "PlayState": handlePlayState(json.data); break
        case "Seek": handleSeekState(json.data); break
        case "Error": handleError(json.data); break
        default: console.log(`Received unhandled ws message of type ${json.type}!`)
    }
}

function handleRoomInfo(data: any) {
    const info: RoomInfo = data.room
    roomInfo = info
    myId = data.yourId
    info.users.forEach(u => createUser(u.name, u.uuid))
    if (info.video != undefined) {
        handleSetVideo(info.video)
        syncVideoWithRoomInfo(info)
    }
    if (info.waiting) {
        waitingPopup.style.display = "block"
    }
}
function handleUserJoin(info: User) {
    createUser(info.name, info.uuid)
}
function handleUserLeave(uuid: string) {
    removeUser(uuid)
}
function handleSetUsername(data: any) {
    const userEl = document.getElementById(data.uuid)!
    userEl.childNodes[0].textContent = data.name
}
function handleSetVideo(url: string) {
    video.src = url
    if (roomInfo.waiting) {
        roomInfo.waiting = false
        waitingPopup.style.display = "none"
    }
}
function handleWait() {
    roomInfo.waiting = true
    waitingPopup.style.display = "block"
}
function handleError(msg: string) {
    errorPopup.style.display = "block"
    document.getElementById("error-msg")!.innerText = msg
}
function errorPopupClose() {
    errorPopup.style.display = "none"
}
function handlePlayState(state: boolean) {
    roomInfo.playing = state
    remotePlayCommand = true
    if (state) {
        const promise = video.play()
        if (promise !== undefined) {
            promise.then(_ => {
                // autoplay started
            }).catch(error => {
                // not allowed
            })
        }
    }
    else
        video.pause()
}
function handleSeekState(time: number) {
    remotePlayCommand = true
    video.currentTime = time / 1000
}

function syncVideoWithRoomInfo(info: RoomInfo) {
    if (info.playing) {
        remotePlayCommand = true
        video.currentTime = (info.progress + Date.now() - info.playingSince) / 1000
        if (!popupShown) {
            video.play()
        }
    }
    else {
        const roomProgress = info.progress / 1000
        if (roomProgress != video.currentTime) {
            remotePlayCommand = true
            video.currentTime = info.progress / 1000
        }
    }
}

function createPacket(type: string, data: any) {
    const packet = {
        "type": type,
        "data": data
    }
    console.log("creating packet:", packet)
    return JSON.stringify(packet)
}

function share() {
    document.getElementById("share")!.innerText = "Copied!"
    navigator.clipboard.writeText(window.location.href)
}

function submitVideo() {
    webSocket.send(createPacket("SetVideo", urlInput.value ))
    urlInput.value = ""
}

function autoplayPopupShow() {
    autoplayPopup.style.display = "block"
}
function autoplayPopupClose() {
    autoplayPopup.style.display = "none"
    popupShown = false
    syncVideoWithRoomInfo(roomInfo)
}

function getUsername() {
    const cookieUsername = getCookie("username")

    if (cookieUsername == "") {
        // Generate a new username
        const username = "user-" + generateUserID(4)
        createCookie("username", username, 365)
        return username
    } else {
        return cookieUsername
    }
}
function handleUserProgress(progressInfo: any) {
    const progressElem = document.getElementById(progressInfo.uuid + "-progress")!
    progressElem.innerText = formatTime(progressInfo.progress)
    const user = document.getElementById(progressInfo.uuid)!
    if (Math.abs(progressInfo.progress - video.currentTime) > 2) {
        if (!user.classList.contains("off-sync"))
            user.classList.add("off-sync")
    } else {
        if (user.classList.contains("off-sync"))
            user.classList.remove("off-sync")
    }
}
function formatTime(number: number) {
    return String(Math.floor(number / 60)).padStart(2, '0') + ":" + String(Math.floor(number % 60)).padStart(2, '0')
}

function createUser(name: string, uuid: string) {
    const usersContainer = document.getElementById("users")!
    const newUser = document.createElement("div")
    newUser.innerText = name
    newUser.className = "user"
    newUser.id = uuid
    const progress = document.createElement("p")
    progress.innerText = "00:00"
    progress.id = uuid + "-progress"
    newUser.appendChild(progress)
    usersContainer.appendChild(newUser)
}

function removeUser(uuid: string) {
    const user = document.getElementById(uuid)!
    user.remove()
}

function createCookie(name: string, value: string | number | boolean, days: number) {
    const date = new Date()
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000))
    document.cookie = name + "=" + encodeURIComponent(value) + "; expires=" + date.toUTCString() + "; SameSite=Lax; path=/"
}

function getCookie(name: string) {
    if (document.cookie.length > 0) {
        let startIndex = document.cookie.indexOf(name + "=")
        if (startIndex != -1) {
            startIndex += name.length + 1
            let endIndex = document.cookie.indexOf(";", startIndex)
            if (endIndex == -1) {
                endIndex = document.cookie.length
            }
            return decodeURIComponent(document.cookie.substring(startIndex, endIndex))
        }
    }
    return ""
}

function generateUserID(length: number) {
    let result = ""
    const characters = "0123456789"
    let counter = 0
    while (counter < length) {
      result += characters.charAt(Math.floor(Math.random() * characters.length))
      counter += 1
    }
    return result
}