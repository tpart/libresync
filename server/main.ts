import { Application, Router } from "https://deno.land/x/oak@v16.0.0/mod.ts"
import { compileFile } from "https://deno.land/x/pug@v0.1.6/mod.ts"
import { cryptoRandomString } from "https://deno.land/x/crypto_random_string@1.0.0/mod.ts"
import { parseArgs } from "jsr:@std/cli/parse-args";
import { getVideoSource, isKeySource } from "./video.ts"
import {
  addWs,
  createRoomIfNotExists,
  getRoom,
  removeUser,
  removeWs,
  sendRoomInfo,
  sendRoomPlayState,
  sendRoomVideo,
  sendUserJoin,
  sendSeek,
  User,
  uuid2roomName,
  ws2uuid,
  getVideoRessource,
  setVideoRessource,
  removeVideoRessource,
  sendWait,
  sendError,
  sendUserProgressUpdate,
  getUser,
  sendUsernameUpdate,
} from "./room.ts"

const flags = parseArgs(Deno.args, {
  boolean: ["allow-youtube"],
  string: ["port"]
})

const port = flags.port == undefined ? 8080 : Number(flags.port)

const main = compileFile("client/main.pug")
const room = compileFile("client/room.pug")

const app = new Application()
const r = new Router()

r.get("/", (ctx) => {
  ctx.response.body = main()
})

r.get("/room/:param", (ctx) => {
  const roomName = ctx.params["param"]

  ctx.response.body = room({
    room: roomName,
  })
})

r.get("/video/:param", async (ctx) => {
  const ressource = getVideoRessource(ctx.params["param"])
  
  if (typeof ressource == "undefined") {
    ctx.response.status = 404
    ctx.response.body = "not found"
  } else {
    await ctx.send({path: ressource, root: Deno.cwd()})
  }
})

r.get("/wss", (ctx) => {
  if (!ctx.isUpgradable) {
    ctx.throw(501)
  }
  const ws = ctx.upgrade()
  ws.onopen = () => {
    console.log("Connected to client")
  }
  ws.onmessage = async (m) => {
    console.log("Got message from client: ", m.data)
    const message = JSON.parse(m.data as string)
    switch (message.type) {
      case "JoinRoom": {
        const room = createRoomIfNotExists(message.data.room)
        const user: User = {
          name: message.data.username,
          uuid: crypto.randomUUID(),
          progress: room.progress
        }
        addWs(ws, user.uuid)
        sendRoomInfo(ws, room, user.uuid)
        room.users.push(user)
        sendUserJoin(message.data.room, user)
        break
      }
      case "SetVideo": {
        const roomName = uuid2roomName(ws2uuid(ws)!)!
        const r = getRoom(roomName)

        if (r.waiting) return

        const srcType = isKeySource(message.data)

        if (typeof srcType == "undefined") {
          sendError(roomName, "Invalid URL")
          return
        }

        if (srcType) {
          if (!flags["allow-youtube"]) {
            sendError(roomName, "The provided URL is not supported.")
            return
          }
          r.waiting = true
          sendWait(roomName)
        }

        const sourceUrl = await getVideoSource(message.data)

        if (typeof r.key != "undefined") {
          removeVideoRessource(r.key)
        }

        if (sourceUrl[0] == "url") {
          sendRoomVideo(roomName, sourceUrl[1])
          r.video = sourceUrl[1]
        } else {
          r.key = cryptoRandomString({length: 32, type: 'alphanumeric'})
          setVideoRessource(r.key!, sourceUrl[1])
          const url = "/video/" + r.key!
          sendRoomVideo(roomName, url)
          r.video = url
          r.waiting = false
        }
        break
      }
      case "SetUsername": {
        const uuid = ws2uuid(ws)!
        const roomName = uuid2roomName(uuid)!
        const user = getUser(uuid, roomName)!
        user.name = message.data
        sendUsernameUpdate(roomName, uuid, user.name)
        break
      }
      case "PlayState": {
        const roomName = uuid2roomName(ws2uuid(ws)!)!
        const r = getRoom(roomName)
        if (message.data == true) {
          sendRoomPlayState(roomName, ws2uuid(ws)!, true)
          r.playing = true
          r.playingSince = Date.now()
        } else {
          sendRoomPlayState(roomName, ws2uuid(ws)!, false)
          r.playing = false
          r.progress += Date.now() - r.playingSince
        }
        break
      }
      case "Seek": {
        const roomName = uuid2roomName(ws2uuid(ws)!)!
        const r = getRoom(roomName)
        r.progress = message.data
        r.playingSince = Date.now()
        sendSeek(roomName, ws2uuid(ws)!, message.data)
        break
      }
      case "TimeUpdate": {
        const roomName = uuid2roomName(ws2uuid(ws)!)!
        sendUserProgressUpdate(roomName, ws2uuid(ws)!, message.data)
        break
      }
      default: {
        console.log(`Received unsupported message type: ${message.type}`)
        ws.close()
        break
      }
    }
  }
  ws.onclose = () => {
    const uuid = removeWs(ws)
    if (uuid === undefined) return
    const roomName = uuid2roomName(uuid)
    if (roomName === undefined) return

    removeUser(roomName, uuid)
    console.log(`User ${uuid} disconnected`)
  }
})

app.use(r.routes())
app.use(r.allowedMethods())
app.use(async (ctx, next) => {
  try {
    await ctx.send({
      root: `${Deno.cwd()}/public`,
    })
  } catch {
    await next()
  }
})

app.use((ctx) => {
  ctx.response.status = 301
  ctx.response.headers.append("Location", "/")
})

console.log(`Server started on port ${port}`)
await app.listen({ port: port })
