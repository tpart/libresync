export function isKeySource(urlString: string): boolean | undefined {
    try {
        const url = new URL(urlString)

        if (url.pathname == "/watch" || url.host == "www.youtube.com")
            return true
        return false
    } catch {
        return undefined
    }
}

export async function getVideoSource(urlString: string): Promise<["file", string] | ["url", string]> {
    const url = new URL(urlString)
    const filename = crypto.randomUUID()

    if (isKeySource(urlString)) {
        const command = new Deno.Command("yt-dlp", {
            args: [url.toString(), "--output", filename]
        })
        const { success } = await command.output()
        if (!success) {
            console.error("yt-dlp did not exit successfully!")
            return ["url", url.toString()]
        }

        return ["file", (await getFileWithStart(filename))!]
    }

    return ["url", url.toString()]
}

async function getFileWithStart(start: string) {
    for await(const f of Deno.readDir("./")) {
        if(!f.isFile) continue
        if (f.name.startsWith(start))
            return f.name
    }
}