// deno-lint-ignore-file no-explicit-any
const roomDict: { [key: string]: Room } = {}
const connections: { [key: string]: WebSocket } = {}
const videoKeys: { [key: string]: string } = {}

export interface Room {
    users: User[]
    video: string | undefined
    progress: number
    playing: boolean
    playingSince: number
    key: string | undefined
    waiting: boolean
}

export interface User {
    name: string
    uuid: string
    progress: number
}

export function createRoomIfNotExists(name: string) : Room {
    if (name in roomDict) {
        return roomDict[name]
    }
    const newRoom: Room = {
        users: [],
        video: undefined,
        progress: 0,
        playing: false,
        playingSince: Date.now(),
        key: undefined,
        waiting: false
    }
    roomDict[name] = newRoom
    return newRoom
}

export function addWs(ws: WebSocket, uuid: string) {
    connections[uuid] = ws
}
export function removeWs(ws: WebSocket): string | undefined {
    for(const c in connections) {
        if(connections[c] == ws) {
            delete connections[c]
            return c
        }
    }
}
export function addKeyContent(roomName: string, videoKey: string, filename: string) {
    getRoom(roomName).key = videoKey
    videoKeys[videoKey] = filename
}
export function ws2uuid(ws: WebSocket): string | undefined {
    for(const c in connections) {
        if(connections[c] == ws) {
            return c
        }
    }
}
export function uuid2roomName(uuid: string): string | undefined {
    for(const r in roomDict) {
        for(const u of roomDict[r].users) {
            if(u.uuid == uuid) {
                return r
            }
        }
    }
}
export function getRoom(name: string): Room {
    return roomDict[name]
}
export function getUser(uuid: string, roomName: string) {
    for(const u of roomDict[roomName].users) {
        if(u.uuid == uuid) {
            return u
        }
    }
}
export function setVideoRessource(key: string, filename: string) {
    videoKeys[key] = filename
}
export function getVideoRessource(key: string): string | undefined {
    if (videoKeys[key])
        return videoKeys[key]
}
export function removeVideoRessource(key: string) {
    Deno.remove(videoKeys[key])
    delete videoKeys[key]
}
export function removeUser(roomName: string, uuid: string) {
    roomDict[roomName].users = roomDict[roomName].users.filter(e => e.uuid != uuid)
    if (roomDict[roomName].users.length == 0) {
        console.log(`Deleting room ${roomName}`)
        if (roomDict[roomName].key != undefined) {
            removeVideoRessource(roomDict[roomName].key!)
        }
        delete roomDict[roomName]
    } else {
        sendUserRemove(roomName, uuid)
    }
}
export function sendUserJoin(roomName: string, user: User) {
    sendPacketRoom(roomName, "UserJoin", user)
}
export function sendUsernameUpdate(roomName: string, userUuid: string, newName: string) {
    sendPacketRoom(roomName, "SetUsername", {uuid: userUuid, name: newName})
}
export function sendWait(roomName: string) {
    sendPacketRoom(roomName, "Wait", {})
}
export function sendError(roomName: string, message: string) {
    sendPacketRoom(roomName, "Error", message)
}
export function sendRoomVideo(roomName: string, url: string, removeKey = false) {
    if (removeKey) {
        getRoom(roomName).key = undefined
    }
    sendPacketRoom(roomName, "SetVideo", url)
}
export function sendRoomPlayState(roomName: string, sentByUuid: string, state: boolean) {
    sendPacketRoomExcept(roomName, sentByUuid, "PlayState", state)
}
export function sendSeek(roomName: string, sentByUuid: string, time: number) {
    sendPacketRoomExcept(roomName, sentByUuid, "Seek", time)
}

export function sendUserProgressUpdate(roomName: string, sentByUuid: string, time: number) {
    sendPacketRoomExcept(roomName, sentByUuid, "UserProgress", {uuid: sentByUuid, progress: time})
}
function sendUserRemove(roomName: string, uuid: string) {
    sendPacketRoom(roomName, "UserLeave", uuid)
}
export function sendRoomInfo(ws: WebSocket, r: Room, uuid: string) {
    sendPacket(ws, "RoomInfo", {room: r, yourId: uuid})
}
export function sendPacketRoom(roomName: string, packetType: string, packetData: any) {
    roomDict[roomName].users.forEach(u => sendPacketUuid(u.uuid, packetType, packetData))
}
export function sendPacketRoomExcept(roomName: string, exceptionUuid: string, packetType: string, packetData: any) {
    roomDict[roomName].users.forEach(u => {
        if (u.uuid != exceptionUuid) sendPacketUuid(u.uuid, packetType, packetData)
    })
}
export function sendPacket(ws: WebSocket, packetType: string, packetData: any) {
    const packet = {
        type: packetType,
        data: packetData
    }
    ws.send(JSON.stringify(packet))
}
export function sendPacketUuid(uuid: string, packetType: string, packetData: any) {
    const packet = {
        type: packetType,
        data: packetData
    }
    connections[uuid].send(JSON.stringify(packet))
}