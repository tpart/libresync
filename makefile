.PHONY: watch

run:
	esbuild client/index.ts --outfile=public/index.js &
	esbuild client/room.ts --outfile=public/room.js &
	deno run --allow-read --allow-write --allow-net --allow-env --allow-run=yt-dlp server/main.ts
run-full:
	esbuild client/index.ts --outfile=public/index.js &
	esbuild client/room.ts --outfile=public/room.js &
	deno run --allow-read --allow-write --allow-net --allow-env --allow-run=yt-dlp server/main.ts --allow-youtube
watch:
	esbuild client/index.ts --outfile=public/index.js --watch=forever &
	esbuild client/room.ts --outfile=public/room.js --watch=forever &
	deno run --allow-read --allow-write --allow-net --allow-env --allow-run=yt-dlp server/main.ts --allow-youtube