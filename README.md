# libresync
A libre synced video player, written in TypeScript with Deno using Oak and Pug.
It supports direct video URLs which are playable in the browser (e.g. webm videos) as well as additional websites such as YouTube.
![Preview](/preview.png)

## Usage
This project requires `deno`, `esbuild` and `make`. If you want libresync to support additional websites such as YouTube, install `yt-dlp` as well. On Arch, these packages can be installed with:
```
pacman -S --needed deno esbuild make yt-dlp
```
To run the server **without support for additional websites**, use
```
make run
```
Alternatively, to run the server **with support for additional websites**, use
```
make run-full
```
**Please note that enabling this feature means that your server will then host videos from such sources, which may not be desirable.**

For development purposes, use
```
make watch
```

## License
```
libresync
Copyright (C) 2024 tpart

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```